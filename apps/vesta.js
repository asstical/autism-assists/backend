'use strict'

///////////////////////////////////
///////// Node.js Modules /////////
///////////////////////////////////

// Express frameworks
const express = require('express');
const app = express();
const router = express.Router();

// MongoDB frameworks
const mongo = require('mongodb').MongoClient;
const async = require('async');
const path = require('path');
const ObjectID = require('mongodb').ObjectID;

// Web-pack frameworks
const http = require('http');
const https = require('https');
const assert = require('assert');
const bodyParser = require('body-parser')
const axios = require('axios')
var request = require("request");

// Key and Invite code generators
const TokenGenerator = require('uuid-token-generator');
const tokgen2 = new TokenGenerator(256, TokenGenerator.BASE62);
const shortid = require('shortid');
// Encrypt and decrypt module (https://www.npmjs.com/package/bcrypt)
const bcrypt = require('bcryptjs');
const saltRounds = 12;
const lowerCase = require('lower-case')
const FormData = require('form-data');

// Nodemailer
const nodemailer = require("nodemailer");

//////////////////////////////////////////////////
////////////////// Credentials ///////////////////
//////////////////////////////////////////////////

const mongo_url = 'mongodb://mongodb:27017'
const mongo_user = 'snickies'
const mongo_pwd = 'MvvoocxjjBhsvhYJQKVi7AcZ'

//const db = client.db('thompson');
//const u_col = db.collection('users');
//const p_col = db.collection('patients');
//const l_col = db.collection('logs');
//const e_col = db.collection('events');
//const eac_col = db.collection('external_activities');
//const iac_col = db.collection('internal_activities');

// Withings developer credentials
// For help -> (http://developer.withings.com)
const withings_client_id = 'bb7908cbae608f9a1b9e11e45aabe8b62876518dfc431403dca392c14b489951'
const withings_cleint_secret = 'd611d1034e07d4749ba5d7ebdf5e7a43fc4639422736e38a9bdcfccfa11a0420' 

// Fitbit developer credentials
// For help -> (https://dev.fitbit.com)
const fitbit_cleint_id = '22DM7Z'
const fitbit_client_secret = '5629f1f740a14c0a1c774758232ab036'

//////////////////////////////////////////////////
//////////////////// TEST APIs ///////////////////
//////////////////////////////////////////////////

exports.index = (req,res) => {
    console.log('=> neptune');
    res.json({output: 'neptune'});
}

//////////////////////////////////////////////////
//////////////////// DEBUGGING  //////////////////
//////////////////////////////////////////////////

exports.manual_add_medicine = (req,res) => {
    
    const patient_id = new ObjectID(req.query.pid);
    const key = req.query.key;
    
    const name = (req.query.na);
    const strength = decodeURIComponent(req.query.st);
    const quantity = req.query.qu;
    const time_of_day = req.query.tod;
    
    const comment = decodeURIComponent(req.query.co);
    const lat = req.query.lat;
    const long = req.query.long;
    
    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {
        
        const db = client.db('thompson');
        const u_col = db.collection('users');
        const m_col = db.collection('medicines');
        const e_col = db.collection('events');
        
        u_col.findOne({key: key}, (err, query) => {
         
            console.log(query);
            
            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'log':'user key is invalid'});
            } else {
                console.log('user key is valid')
                
                var patient_medicine = {'patient_id': patient_id, 'name': name, 'strength': strength, 'quantity': quantity, 'time_of_day': time_of_day, 'comment': comment, 'latitude': lat, 'longitude': long, 'date_created': new Date(), 'created': query._id};
                
                m_col.insertOne(patient_medicine, (errr, respond) => {
                    
                    console.log('new record add for patient ' + patient_id)
                    
                    var event_query = {'created_date': new Date().toJSON().slice(0,10), 'patient_id': patient_id};
                    
                    e_col.findOne(event_query,(err, event) => {
                       
                        console.log(event);
                        
                        var event_upsert = { $push: {'medicines': patient_medicine}};
                            
                        e_col.updateMany(event_query, event_upsert, { upsert : true }, (err, respond) => {
                            console.log(respond);
                            res.json({'status': res.statusCode,'return':true,'log':'medicine created & inserted to  event'});
                        });
                        
                    });
                });
                
            }
        });
        
    });
         
};

//////////////////////////////////////////////////
////////////// Authentication APIs ///////////////
//////////////////////////////////////////////////

exports.oauth_fitbit_redirect = (req, res) => {
    
    const requestToken = req.query.code
    console.log("=> Authorize Token: " + requestToken)
    
    let buff = new Buffer(fitbit_cleint_id + ":" + fitbit_client_secret);  
    let basicAuth = buff.toString('base64');
    console.log(basicAuth)
    
    axios({
        // make a POST request
        method: 'post',
        // to the Github authentication API, with the client ID, client secret
        // and request token
        url: "https://api.fitbit.com/oauth2/token?clientId=" + fitbit_cleint_id + "&grant_type=authorization_code&redirect_uri=https%3A%2F%2Fwww.snickies.com%2Fthompson%2Foauth%2Ffitbit%2Fredirect" + "&code=" + requestToken,
        // Set the content type header, so that we get the response in JSOn
        headers: {
            'Authorization': "Basic "+ basicAuth
        }
    }).then((response) => {
        // Once we get the response, extract the access token from
        // the response body
        console.log(response)
        const accessToken = response.data.access_token
        const refreshToken = response.data.refresh_token
        console.log("=> Access Token: " + accessToken)
        console.log("=> Refresh Token: " + refreshToken)
        
        // Save data into MongoDB
        
//        // redirect the user to the welcome page, along with the access token
//        res.redirect(`/welcome.html?access_token=${accessToken}`)
        res.json({authorize_token: requestToken, access_token: accessToken, refresh_token: refreshToken})
    })
    
}

exports.oauth_withings_redirect = (req, res) => {
    
    const requestToken = req.query.code
    console.log("=> Authorize Token: " + requestToken)

    request.post({
    url: 'https://account.withings.com/oauth2/token',
    form: { 
        grant_type: 'authorization_code',
        client_id: withings_client_id,
        client_secret: withings_cleint_secret,
        code: requestToken,
        redirect_uri: 'https://www.snickies.com/thompson/oauth/withings/redirect'
    },
    headers: { 
        'Content-Type' : 'application/x-www-form-urlencoded' 
    },
    method: 'POST'
    }, function (err3, res3) {
        if (err3) throw err3;
            var results = JSON.parse(res3.body);
            var accessToken = results.access_token;
            var refreshToken = results.refresh_token;
            res.json({authorize_token: requestToken, access_token: accessToken, refresh_token: refreshToken})
    });
    
}

//////////////////////////////////////////////////
////////////// Authentication APIs ///////////////
//////////////////////////////////////////////////

exports.generate = (req,res) => {
    
    const password = bcrypt.hashSync(req.query.pass, saltRounds);
    const code = shortid.generate();
    const key = tokgen2.generate()
    
    console.log('password ' + password + ' code ' + code + ' key ' + key)
    res.json({'password': password, 'code': code,'key': key})
}

// POST method: request to invite user into application
exports.user_invite = (req,res) => {
    
    const key = req.query.key;
    const email = req.query.email;
    const role = req.query.role
    
    // Generate invitation code & authentication key
    const invite_code = shortid.generate();
    const auth_key = tokgen2.generate()
    
    var new_user = {email: email, password: null, firstname: null, lastname: null, active: false, consent: false, key: auth_key, role: role, invite_code: invite_code, date_invited: new Date(), date_created: null, patients: [], devices: []}
    
    // MongoDB find if key is valid and make sure that it have either admin or manager permission    
    mongo.connect(mongo_url , {useNewUrlParser: true}, (err, client) => {
        
        const db = client.db('thompson');
        const u_col = db.collection('users');
        
        // Search in MongoDB to make sure that user don't already existed
        u_col.findOne({'email': email}, (err, result) => {
            if(result != null){
                
                // - If already existed send out another email with invitation code
                var transporter = nodemailer.createTransport({
                    host: 'mail.privateemail.com',
                    port: 587,
                    secure: false,
                    auth : {
                        // Privateemail.com by Namecheap.com credentials
                        user: 'hello@autismassists.com',
                        pass: 'gBqwGbn2U6bqTs9MPLt'
                    },
                });
                
                // Nodemailer - log config
                var mailOptions = {
                    from: '"Autism Assists" <hello@autismassists.com>',
                    to: email,
                    subject: "Here is your invitation code for Autism Assists",
                    html: "<h1>Hi again! 👋,</h1><p>You're getting this email because you have been re-invited by the institution to participate in their research 🔬. <p>By using the unique invitation code below.</p><h3>Code: <b>" + result.invite_code + "</b></h3><p>If you have any problems, please don't hesitate to reach out to your institution.</p>Have a great day!<br><p>Luke 👱‍♂️</p>"
                }
                
                transporter.sendMail(mailOptions, function(error, info){
                    if (error) {
                        console.log(error);
                        res.json({'status': res.statusCode, 'return': false,'log': error});
                    } else {
                        console.log('=> Email sent: ' + info.response);
                        res.json({'status': res.statusCode, 'return':true,'log':'found existing invitation and send'});
                    }
                });
                
                
            } else {
                
                // - If doesn't existed send out email with newly generated inivation code
                var transporter = nodemailer.createTransport({
                    host: 'mail.privateemail.com',
                    port: 587,
                    secure: false,
                    auth : {
                        // Privateemail.com by Namecheap.com credentials
                        user: 'hello@autismassists.com',
                        pass: 'gBqwGbn2U6bqTs9MPLt'
                    },
                });
                
                // Nodemailer - log config
                var mailOptions = {
                    from: '"Autism Assists" <hello@autismassists.com>',
                    to: email,
                    subject: "You're invited to Autism Assists, Let's get started!",
                    html: "<h1>Bonjour! 👋,</h1><p>Welcome to <b>Autism Assists</b>, you're getting this email because you have been invited by the institution to participate in their research 🔬.<h3>Code: <b>" + invite_code + "</b></h3><p>If= you have any problems, please don't hesitate to reach out to your institution.</p>Have a great day!<br><p>Luke 👱‍♂️</p>"
                }
                
                transporter.sendMail(mailOptions, function(error, info){
                    if (error) {
                        console.log(error);
                        res.json({'status': res.statusCode, 'return':'false','log': error});
                    } else {
//                         Create inactive empty user with Generate invitation code in users MongoDB
                        u_col.insertOne(new_user, (errr, ans) => {
                            console.log('=> Email sent: ' + info.response);
                            res.json({'status': res.statusCode, 'return':'true','log':'new invitation send'});
                        })
                    }
                });
            }
        })
        
    });
};

// POST method: with specific invitation code user can request to create account
exports.user_signup = (req,res) => {
    
    const invite_code = req.query.code;
    const email = req.query.email;
    const password = bcrypt.hashSync(req.query.pass, saltRounds);
    const phone = decodeURIComponent(req.query.phone);
    const firstname = decodeURIComponent(lowerCase(req.query.fname));
    const lastname = decodeURIComponent(lowerCase(req.query.lname));
    
    var update_user = { $set: {'password': password, 'firstname': firstname, 'lastname': lastname, 'phone': phone, 'active': true, 'date_created': new Date()}};
    
    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {
        
        const db = client.db('thompson');
        const u_col = db.collection('users');
    
        u_col.findOne({'email': email, 'invite_code': invite_code}, (err, query) => {

            if(query == null) {
                console.log(query);
                res.json({'status': res.statusCode,'return': false,'messsage':'user with this email does not existed'});
            } else {
                console.log(query);
                console.log(query._id);

                u_col.updateMany({'_id':query._id}, update_user, (errr, respond) => {
                    console.log(respond);
                    res.json({'status': res.statusCode,'return': true,'messsage':'registed user ' + email});
                });
            }
        })
    });
};

// GET method, user authentication to obtain access key
// https://www.snickies.com/thompson/user/signin
exports.user_signin = (req,res) => {
    
    const email = req.query.email;
    const password = req.query.pass;
    
    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {
    
        const db = client.db('thompson');
        const u_col = db.collection('users');
        
        u_col.findOne({'email': email}, (err, query) => {
           
            console.log(query);
            
            if(query == null) {
                res.json({'status': res.statusCode,'return': false,'messsage':'user with this email does not existed'});
            } else {
                
                if(bcrypt.compareSync(password, query.password) == false){
                    res.json({'status': res.statusCode,'return': false,'messsage':'wrong password'});
                } else if(bcrypt.compareSync(password, query.password) == true) {
                    res.json({'status': res.statusCode,'return': true,'messsage':query.key});
                } else {
                    res.json({'status': res.statusCode,'return': false,'messsage':'something went wrong'});
                }
            
            }
            
        });
    
    });
};

// WORK IN PROGRESS
// POST method, user request to reset password
exports.user_forgot =  (req,res) => {
    
    const email = req.query.email;
    
    
};

//////////////////////////////////////////////////
////////////// Events & Records APIs /////////////
//////////////////////////////////////////////////

// WORK IN PROGRESS
// GET & POST method, for GET it will request log from specified user, for POST it will post log to specified user
exports.user_post_log = (req,res) => {
    
};

// WORK IN PROGRESS
// GET & POST method, for GET it will request log from specified user, for POST it will post log to specified user
exports.user_get_log = (req,res) => {
    
};

// WORK IN PROGRESS
// GET & POST method, for GET it will retrieve user information, for POST it will update user information
exports.user_post_info = (req,res) => {
    
};

// GET & POST method, for GET it will retrieve user information, for POST it will update user information
exports.user_get_info = (req,res) => {
    
//    const user_id = new ObjectID(req.query.uid);
    const key = req.query.key;
    
    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {
        
        const db = client.db('thompson');
        const u_col = db.collection('users');
        
        var user_query = {'key': key}
        
        u_col.findOne({key: key}, (err, query) => {
         
            console.log(query);
            
            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'message':'user key is invalid'});
            } else {
                console.log('user key is valid')
                
                u_col.findOne(user_query, (err, result) => {
                    
                    if(result == null){
                        console.log('no paitient was found');
                        res.json({'status': res.statusCode, 'return': false, 'message': 'no paitient was found'})
                    } else {
                        console.log(result);
                        res.json({'status': res.statusCode, 'return': true, 'data': result})
                    }
                });
                
            }
        });
        
    });
};

// POST method create a new patient
exports.patient_signup = (req,res) => {
    
    const key = req.query.key;
    const firstname = lowerCase(req.query.fname);
    const lastname = lowerCase(req.query.lname);
    const nickname = lowerCase(req.query.nname);
    const blood = lowerCase(req.query.blood)
    const identification = lowerCase(req.query.id);
    const institution = lowerCase(req.query.ins);
    const doctor = lowerCase(req.query.doc);
    
    var user_id = '';
    
    mongo.connect(mongo_url , {useNewUrlParser: true}, (err, client) => {
        
        const db = client.db('thompson');
        const u_col = db.collection('users');
        
        u_col.findOne({key: key}, (err, query) => {
            console.log(query);
            
            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'message':'user key is invalid'});
            } else {
                user_id = query._id;
                console.log(user_id)
                
                const p_col = db.collection('patients');
                
//                var new_patient = {firstname: firstname, lastname: lastname, nickname: nickname, identification: identification, dob: new Date(), institution: institution, doctor: doctor, active: true, consent: false, created: user_id, date_created: new Date(), events: [], activities: [], sleeps: [], medications: [], devices: []}
                
                var new_patient = {firstname: firstname, lastname: lastname, nickname: nickname, identification: identification, dob: new Date(), blood: blood,institution: institution, doctor: doctor, active: true, consent: false, created: user_id, date_created: new Date()}
        
                p_col.insertOne(new_patient, (err, respond) => {
                    console.log(respond)
                    res.json({'status': res.statusCode, 'return':true,'message':'add new patient by ' + user_id});
                })
            }
        })
    });
};

// POST method link patient to specific user
exports.patient_link = (req,res) => {
    
    const patient_id = new ObjectID(req.query.pid);
    const user_id = new ObjectID(req.query.uid);
    const key = req.query.key;
    
    var insert_patient = { $push: {'patients': patient_id}};
    
    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {
    
        const db = client.db('thompson');
        const u_col = db.collection('users');
        
         u_col.findOne({key: key}, (err, query) => {
         
            console.log(query);
            
            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'log':'user key is invalid'});
            } else {
                
                u_col.updateOne({'_id': query._id}, insert_patient, (errr, respond) => {
                    console.log(respond);
                    res.json({'status': res.statusCode, 'return':true,'log':'patinet ' + patient_id + ' add to user ' + user_id});
                })
            }
         });
    });
};

// POST method add activity to sleeps and events
exports.add_activity = (req,res) => {
    
    const patient_id = new ObjectID(req.query.pid);
    const key = req.query.key;
    
    const type = req.query.type;
    const subtype = req.query.subtype;
    const rating = req.query.rating;
    const rating_des = req.query.rating_des;
    const comment = decodeURIComponent(req.query.comment);
    const lat = req.query.lat;
    const long = req.query.long;
    
    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {
    
        const db = client.db('thompson');
        const u_col = db.collection('users');
        const a_col = db.collection('activities');
        const e_col = db.collection('events');
        
        u_col.findOne({key: key}, (err, query) => {
         
            console.log(query);
            
            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'log':'user key is invalid'});
            } else {
                console.log('user key is valid')
                
                var patient_activity = {'patient_id': patient_id, 'type': type, 'subtype': subtype, 'rating': rating, 'rating_description': rating_des, 'comment': comment, 'latitude': lat, 'longitude': long, 'date_created': new Date(), 'created': query._id};
                
                a_col.insertOne(patient_activity, (errr, respond) => {
                    
                    console.log('new record add for patient ' + patient_id)
                    
                    var event_query = {'created_date': new Date().toJSON().slice(0,10), 'patient_id': patient_id};
                    
                    e_col.findOne(event_query,(err, event) => {
                       
                        console.log(event);
                        
                        var event_upsert = { $push: {'activities': patient_activity}};
                            
                        e_col.updateMany(event_query, event_upsert, { upsert : true }, (err, respond) => {
                            console.log(respond);
                            res.json({'status': res.statusCode,'return':true,'log':'activity created & inserted to  event'});
                        });
                        
                    });
                });
                
            }
        });
    });
    
};

// POST method add sleep to activities and events
exports.add_sleep = (req,res) => {
    
    const patient_id = new ObjectID(req.query.pid);
    const key = req.query.key;
    
    const duration = req.query.du;
    const depth = req.query.de;
    const interuptions = req.query.inter;
    const snoring = req.query.sn;
    const time_to_sleep = req.query.tts;
    const time_to_up = req.query.ttu;
    const comment = decodeURIComponent(req.query.co);
    const lat = req.query.lat;
    const long = req.query.long;
    
    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {
        
        const db = client.db('thompson');
        const u_col = db.collection('users');
        const s_col = db.collection('sleeps');
        const e_col = db.collection('events');
        
        u_col.findOne({key: key}, (err, query) => {
         
            console.log(query);
            
            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'log':'user key is invalid'});
            } else {
                console.log('user key is valid')
                
                var patient_sleep = {'patient_id': patient_id, 'duration': duration, 'depth': depth, 'interuptions': interuptions, 'snoring': snoring, 'time_to_sleep': time_to_sleep, 'time_to_up': time_to_up, 'comment': comment, 'latitude': lat, 'longitude': long, 'date_created': new Date(), 'created': query._id};
                
                s_col.insertOne(patient_sleep, (errr, respond) => {
                    
                    console.log('new record add for patient ' + patient_id)
                    
                    var event_query = {'created_date': new Date().toJSON().slice(0,10), 'patient_id': patient_id};
                    
                    e_col.findOne(event_query,(err, event) => {
                       
                        console.log(event);
                        
                        var event_upsert = { $push: {'sleeps': patient_sleep}};
                            
                        e_col.updateMany(event_query, event_upsert, { upsert : true }, (err, respond) => {
                            console.log(respond);
                            res.json({'status': res.statusCode,'return':true,'log':'sleep created & inserted to  event'});
                        });
                        
                    });
                });
                
            }
        });
        
    });
         
};
    
// POST method add medicine to medicines and events
exports.add_medicine = (req,res) => {
    
    const patient_id = new ObjectID(req.query.pid);
    const key = req.query.key;
    
    const name = (req.query.na);
    const strength = decodeURIComponent(req.query.st);
    const quantity = req.query.qu;
    const time_of_day = req.query.tod;
    
    const comment = decodeURIComponent(req.query.co);
    const lat = req.query.lat;
    const long = req.query.long;
    
    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {
        
        const db = client.db('thompson');
        const u_col = db.collection('users');
        const m_col = db.collection('medicines');
        const e_col = db.collection('events');
        
        u_col.findOne({key: key}, (err, query) => {
         
            console.log(query);
            
            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'log':'user key is invalid'});
            } else {
                console.log('user key is valid')
                
                var patient_medicine = {'patient_id': patient_id, 'name': name, 'strength': strength, 'quantity': quantity, 'time_of_day': time_of_day, 'comment': comment, 'latitude': lat, 'longitude': long, 'date_created': new Date(), 'created': query._id};
                
                m_col.insertOne(patient_medicine, (errr, respond) => {
                    
                    console.log('new record add for patient ' + patient_id)
                    
                    var event_query = {'created_date': new Date().toJSON().slice(0,10), 'patient_id': patient_id};
                    
                    e_col.findOne(event_query,(err, event) => {
                       
                        console.log(event);
                        
                        var event_upsert = { $push: {'medicines': patient_medicine}};
                            
                        e_col.updateMany(event_query, event_upsert, { upsert : true }, (err, respond) => {
                            console.log(respond);
                            res.json({'status': res.statusCode,'return':true,'log':'medicine created & inserted to  event'});
                        });
                        
                    });
                });
                
            }
        });
        
    });
         
};

// GET method to retrieve specific patient informaiton
exports.patient_info = (req,res) => {
    
    const patient_id = new ObjectID(req.query.pid);
    const key = req.query.key;
    
    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {
        
        const db = client.db('thompson');
        const u_col = db.collection('users');
        const p_col = db.collection('patients');
        
        var patient_query = {'_id': patient_id};
        
        u_col.findOne({key: key}, (err, query) => {
         
            console.log(query);
            
            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'message':'user key is invalid'});
            } else {
                console.log('user key is valid')
                
                p_col.findOne(patient_query, (err, result) => {
                    
                    if(result == null){
                        console.log('no paitient was found');
                        res.json({'status': res.statusCode, 'return': false, 'message': 'no paitient was found'})
                    } else {
                        console.log(result);
                        res.json({'status': res.statusCode, 'return': true, 'data': result})
                    }
                });
                
            }
        });
        
    });
    
};

// GET method to retrieve specific patient events
exports.patient_events = (req,res) => {
    
    const patient_id = new ObjectID(req.query.pid);
    const key = req.query.key;
    
    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {
        
        const db = client.db('thompson');
        const u_col = db.collection('users');
        const e_col = db.collection('events');
        
        var patient_query = {'patient_id': patient_id};
        
        u_col.findOne({key: key}, (err, query) => {
         
            console.log(query);
            
            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'message':'user key is invalid'});
            } else {
                console.log('user key is valid')
                e_col.find(patient_query).toArray(function(err, events) {
                    
                    if(events == null){
                        res.json({'status': res.statusCode, 'return': false, 'message': 'cannot find events for this patient'})
                    } else {
                        console.log(events);
                        res.json({'status': res.statusCode, 'return': true, 'data': events})
                    }
                });
            }
        });
        
    });
    
};