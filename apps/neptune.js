'use strict'

///////////////////////////////////
///////// Node.js Modules /////////
///////////////////////////////////

// Express frameworks
const express = require('express');
const app = express();
const router = express.Router();

// MongoDB frameworks
const mongo = require('mongodb').MongoClient;
const async = require('async');
const path = require('path');
const ObjectID = require('mongodb').ObjectID;

// Web-pack frameworks
const http = require('http');
const https = require('https');
const assert = require('assert');
const bodyParser = require('body-parser')
const axios = require('axios')
var request = require("request");

// Key and Invite code generators
const TokenGenerator = require('uuid-token-generator');
const tokgen2 = new TokenGenerator(256, TokenGenerator.BASE62);
const shortid = require('shortid');
// Encrypt and decrypt module (https://www.npmjs.com/package/bcrypt)
const bcrypt = require('bcryptjs');
const saltRounds = 12;
const lowerCase = require('lower-case')
const FormData = require('form-data');

// Nodemailer
const nodemailer = require("nodemailer");

// OS
var os = require('os');

//////////////////////////////////////////////////
////////////////// Credentials ///////////////////
//////////////////////////////////////////////////

const mongo_url = 'mongodb://mongodb:27017'
const mongo_user = 'snickies'
const mongo_pwd = 'MvvoocxjjBhsvhYJQKVi7AcZ'

//const db = client.db('thompson');
//const u_col = db.collection('users');
//const p_col = db.collection('patients');
//const l_col = db.collection('logs');
//const e_col = db.collection('events');
//const eac_col = db.collection('external_activities');
//const iac_col = db.collection('internal_activities');

// Withings developer credentials
// For help -> (http://developer.withings.com)
const withings_client_id = 'bb7908cbae608f9a1b9e11e45aabe8b62876518dfc431403dca392c14b489951'
const withings_cleint_secret = 'd611d1034e07d4749ba5d7ebdf5e7a43fc4639422736e38a9bdcfccfa11a0420'

// Fitbit developer credentials
// For help -> (https://dev.fitbit.com)
const fitbit_cleint_id = '22DM7Z'
const fitbit_client_secret = '5629f1f740a14c0a1c774758232ab036'

//////////////////////////////////////////////////
//////////////////// TEST APIs ///////////////////
//////////////////////////////////////////////////

exports.index = (req,res) => {
    console.log('=> neptune');
    res.json({output: 'neptune'});
}

//////////////////////////////////////////////////
//////////////////// DEBUGGING  //////////////////
//////////////////////////////////////////////////



//////////////////////////////////////////////////
////////////// Authentication APIs ///////////////
//////////////////////////////////////////////////

exports.oauth_fitbit_redirect = (req, res, next) => {

    const requestToken = req.query.code
    console.log("=> Authorize Token: " + requestToken)

    let buff = new Buffer(fitbit_cleint_id + ":" + fitbit_client_secret);
    let basicAuth = buff.toString('base64');
    console.log(basicAuth)

    axios({
        // make a POST request
        method: 'post',
        // to the Github authentication API, with the client ID, client secret
        // and request token
        url: "https://api.fitbit.com/oauth2/token?clientId=" + fitbit_cleint_id + "&grant_type=authorization_code&redirect_uri=https%3A%2F%2Fwww.snickies.com%2Fthompson%2Foauth%2Ffitbit%2Fredirect" + "&code=" + requestToken,
        // Set the content type header, so that we get the response in JSOn
        headers: {
            'Authorization': "Basic "+ basicAuth
        }
    }).then((response) => {
        // Once we get the response, extract the access token from
        // the response body
        console.log(response)
        const accessToken = response.data.access_token
        const refreshToken = response.data.refresh_token
        console.log("=> Access Token: " + accessToken)
        console.log("=> Refresh Token: " + refreshToken)

        // Save data into MongoDB

//        // redirect the user to the welcome page, along with the access token
//        res.json({authorize_token: requestToken, access_token: accessToken, refresh_token: refreshToken})
        res.redirect("/welcome.html?request=" + requestToken + "&access=" + accessToken + "&refresh=" + refreshToken)
    })

}

exports.oauth_withings_redirect = (req, res, next) => {

    const requestToken = req.query.code
    console.log("=> Authorize Token: " + requestToken)

    request.post({
    url: 'https://account.withings.com/oauth2/token',
    form: {
        grant_type: 'authorization_code',
        client_id: withings_client_id,
        client_secret: withings_cleint_secret,
        code: requestToken,
        redirect_uri: 'https://www.snickies.com/thompson/oauth/withings/redirect'
    },
    headers: {
        'Content-Type' : 'application/x-www-form-urlencoded'
    },
    method: 'POST'
    }, function (err3, res3) {
        if (err3) throw err3;
            var results = JSON.parse(res3.body);
            var accessToken = results.access_token;
            var refreshToken = results.refresh_token;
//            res.json({authorize_token: requestToken, access_token: accessToken, refresh_token: refreshToken})
            res.redirect("/welcome.html?request=" + requestToken + "&access=" + accessToken + "&refresh=" + refreshToken)
    });

}

//////////////////////////////////////////////////
////////////// Authentication APIs ///////////////
//////////////////////////////////////////////////

exports.generate = (req,res, next) => {

    const password = bcrypt.hashSync(req.query.pass, saltRounds);
    const code = shortid.generate();
    const key = tokgen2.generate()

    console.log('password ' + password + ' code ' + code + ' key ' + key)
    res.json({'password': password, 'code': code,'key': key})
}

// POST method: request to invite user into application
exports.user_invite = (req,res, next) => {

    const key = req.query.key;
    const email = req.query.email;
    const role = req.query.role

    // Generate invitation code & authentication key
    const invite_code = shortid.generate();
    const auth_key = tokgen2.generate()

    var new_user = {email: email, password: null, firstname: null, lastname: null, active: false, consent: false, key: auth_key, role: role, invite_code: invite_code, date_invited: new Date(), date_created: null, patients: []}

    // MongoDB find if key is valid and make sure that it have either admin or manager permission
    mongo.connect(mongo_url , {useNewUrlParser: true}, (err, client) => {

        const db = client.db('thompson');
        const u_col = db.collection('users');

        // Search in MongoDB to make sure that user don't already existed
        u_col.findOne({'email': email}, (err, result) => {
            if(result != null){

                // - If already existed send out another email with invitation code
                var transporter = nodemailer.createTransport({
                    host: "mail.privateemail.com",
                    port: 587,
                    secure: false,
//                    requireTLS: true,
                    auth : {
                        // Privateemail.com by Namecheap.com credentials
                        user: "hello@autismassists.com",
                        pass: "V2jT*6kZHNw?2VhCVv"
                    }
                });

                // Nodemailer - log config
                var mailOptions = {
                    from: '"Autism Assists" <hello@autismassists.com>',
                    to: email,
                    subject: "Here is your invitation code for Autism Assists",
                    html: "<h1>Hi again! 👋,</h1><p>You're getting this email because you have been re-invited to participate in their research 🔬. <p>By using the unique invitation code below.</p><h3>Code: <b>" + result.invite_code + "</b></h3><p>If you have any problems, please don't hesitate to reach out to your institution.</p>Have a great day!<br><p>Luke 👱‍♂️</p>"
                }

                transporter.sendMail(mailOptions, function(error, info){
                    if (error) {
                        console.log(error);
                        res.json({'status': res.statusCode, 'return': false,'log': error});
                    } else {
                        console.log('=> Email sent: ' + info.response);
                        res.json({'status': res.statusCode, 'return':true,'log':'found existing invitation and send'});
                    }
                });


            } else {

                // - If doesn't existed send out email with newly generated inivation code
                var transporter = nodemailer.createTransport({
                    host: "mail.privateemail.com",
                    port: 587,
                    secure: false,
//                    requireTLS: true,
                    auth : {
                        // Privateemail.com by Namecheap.com credentials
                        user: "hello@autismassists.com",
                        pass: "V2jT*6kZHNw?2VhCVv"
                    }
                });

                // Nodemailer - log config
                var mailOptions = {
                    from: '"Autism Assists" <hello@autismassists.com>',
                    to: email,
                    subject: "You're invited to Autism Assists, Let's get started!",
                    html: "<h1>Bonjour! 👋,</h1><p>Welcome to <b>Autism Assists</b>, you're getting this email because you have been invited to participate in their research 🔬.<h3>Code: <b>" + invite_code + "</b></h3><p>You can started by visiting our website <a href='autismassists.com' target='_blank'>autismassists.com</a> for downloading the app and instruction for sign up within the app using the invite code.</p><p>If you have any problems, please don't hesitate to reach out to us at help@autismassists.com if you have any question or help with technical problems.</p>Have a great day!<br><p>Luke 👱‍♂️</p>"
                }

                transporter.sendMail(mailOptions, function(error, info){
                    if (error) {
                        console.log(error);
                        res.json({'status': res.statusCode, 'return':'false','log': error});
                    } else {
//                         Create inactive empty user with Generate invitation code in users MongoDB
                        u_col.insertOne(new_user, (errr, ans) => {
                            console.log('=> Email sent: ' + info.response);
                            res.json({'status': res.statusCode, 'return':'true','log':'new invitation send'});
                        })
                    }
                });
            }
        })

    });
};

// POST method: with specific invitation code user can request to create account
exports.user_signup = (req,res, next) => {

    const invite_code = req.query.code;
    const email = req.query.email;
    const password = bcrypt.hashSync(req.query.pass, saltRounds);
    const phone = decodeURIComponent(req.query.phone);
    const firstname = decodeURIComponent(lowerCase(req.query.fname));
    const lastname = decodeURIComponent(lowerCase(req.query.lname));

    var update_user = { $set: {'password': password, 'firstname': firstname, 'lastname': lastname, 'phone': phone, 'active': true, 'date_created': new Date()}};

    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {

        const db = client.db('thompson');
        const u_col = db.collection('users');

        u_col.findOne({'email': email, 'invite_code': invite_code}, (err, query) => {

            if(query == null) {
                console.log(query);
                res.json({'status': res.statusCode,'return': false,'messsage':'user with this email does not existed'});
            } else {
                console.log(query);
                console.log(query._id);

                u_col.updateMany({'_id':query._id}, update_user, (errr, respond) => {
                    console.log(respond);
                    res.json({'status': res.statusCode,'return': true,'messsage':'registed user ' + email});
                });
            }
        })
    });
};

// GET method, user authentication to obtain access key
// https://www.snickies.com/thompson/user/signin
exports.user_signin = (req,res, next) => {

    const email = req.query.email;
    const password = req.query.pass;

    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {

        const db = client.db('thompson');
        const u_col = db.collection('users');

        u_col.findOne({'email': email}, (err, query) => {

            console.log(query);

            if(query == null) {
                res.json({'status': res.statusCode,'return': false,'messsage':'user with this email does not existed'});
            } else {

                if(bcrypt.compareSync(password, query.password) == false){
                    res.json({'status': res.statusCode,'return': false,'messsage':'wrong password'});
                } else if(bcrypt.compareSync(password, query.password) == true) {
                    res.json({'status': res.statusCode,'return': true,'role': query.role,'messsage':query.key});
                } else {
                    res.json({'status': res.statusCode,'return': false,'role': query.role,'messsage':'something went wrong'});
                }

            }

        });

    });
};

// WORK IN PROGRESS
// POST method, user request to reset password
exports.user_forgot =  (req,res, next) => {

    const email = req.query.email;


};

//////////////////////////////////////////////////
////////////// Events & Records APIs /////////////
//////////////////////////////////////////////////

// WORK IN PROGRESS
// GET & POST method, for GET it will request log from specified user, for POST it will post log to specified user
exports.user_post_log = (req,res, next) => {

};

// WORK IN PROGRESS
// GET & POST method, for GET it will request log from specified user, for POST it will post log to specified user
exports.user_get_log = (req,res, next) => {

};

// WORK IN PROGRESS
// GET & POST method, for GET it will retrieve user information, for POST it will update user information
exports.user_post_info = (req,res, next) => {

};

// GET & POST method, for GET it will retrieve user information, for POST it will update user information
exports.user_get_info = (req,res, next) => {

//    const user_id = new ObjectID(req.query.uid);
    const key = req.query.key;

    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {

        const db = client.db('thompson');
        const u_col = db.collection('users');

        var user_query = {'key': key}

        u_col.findOne({key: key}, (err, query) => {

            console.log(query);

            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'message':'user key is invalid'});
            } else {
                console.log('user key is valid')

                u_col.findOne(user_query, (err, result) => {

                    if(result == null){
                        console.log('no paitient was found');
                        res.json({'status': res.statusCode, 'return': false, 'message': 'no paitient was found'})
                    } else {
                        console.log(result);
                        res.json({'status': res.statusCode, 'return': true, 'data': result})
                    }
                });

            }
        });

    });
};

// POST method create a new patient
exports.patient_signup = (req,res, next) => {

    const key = req.query.key;
    const firstname = lowerCase(req.query.fname);
    const lastname = lowerCase(req.query.lname);
    const nickname = lowerCase(req.query.nname);
    const dob = req.query.dob;
    const gender = lowerCase(req.query.gender);
    const blood = lowerCase(req.query.blood)
    const identification = lowerCase(req.query.id);
    const institution = lowerCase(req.query.ins);
    const doctor = lowerCase(req.query.doc);

    var user_id = '';

    mongo.connect(mongo_url , {useNewUrlParser: true}, (err, client) => {

        const db = client.db('thompson');
        const u_col = db.collection('users');
        const d_col = db.collection('providers');
        const p_col = db.collection('patients');

        u_col.findOne({key: key}, (err, query) => {
//            console.log(query);

            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'message':'user key is invalid'});
            } else {
                user_id = query._id;
                console.log(user_id)

                var new_patient = {firstname: firstname, lastname: lastname, nickname: nickname, identification: identification, dob: dob, gender: gender, blood: blood,institution: institution, doctor: doctor, active: true, consent: false, created: user_id, date_created: new Date()}

                p_col.insertOne(new_patient, (err, respond) => {

                    console.log(respond)

                    var new_provider = {patient_id: respond.insertedId, date_created: new Date(), created: user_id, providers: []}

                    d_col.insertOne(new_provider, (err, ans) => {

//                        console.log(ans)
                        res.json({'status': res.statusCode, 'return':true,'message':'add new patient by ' + user_id});

                    });
                })
            }
        })
    });
};

// POST method link patient to specific user
exports.patient_link = (req,res, next) => {

    const patient_id = new ObjectID(req.query.pid);
    const user_id = new ObjectID(req.query.uid);
    const key = req.query.key;

    var insert_patient = { $push: {'patients': patient_id}};

    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {

        const db = client.db('thompson');
        const u_col = db.collection('users');

         u_col.findOne({key: key}, (err, keyRes) => {

            console.log(keyRes);

            if(keyRes == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'log':'user key is invalid'});
            } else {
                u_col.updateOne({'_id': user_id}, insert_patient, (errr, respond) => {
                    console.log(respond);
                    res.json({'status': res.statusCode, 'return':true,'log':'patinet ' + patient_id + ' add to user ' + user_id});
                })
            }
         });
    });
};

// POST method link patient to specific user
exports.patient_unlink = (req,res, next) => {

    const patient_id = new ObjectID(req.query.pid);
    const user_id = new ObjectID(req.query.uid);
    const key = req.query.key;

    var insert_patient = { $push: {'patients': patient_id}};

    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {

        const db = client.db('thompson');
        const u_col = db.collection('users');

         u_col.findOne({key: key}, (err, keyRes) => {

            console.log(keyRes);

            if(keyRes == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'log':'user key is invalid'});
            } else {
                u_col.updateOne({'_id': user_id}, insert_patient, (errr, respond) => {
                    console.log(respond);
                    res.json({'status': res.statusCode, 'return':true,'log':'patinet ' + patient_id + ' add to user ' + user_id});
                })
            }
         });
    });
};

// POST method add activity to sleeps and events
exports.add_activity = (req,res, next) => {

    const patient_id = new ObjectID(req.query.pid);
    const key = req.query.key;
    const type = req.query.type;
    const subtype = req.query.subtype;
    const rating = req.query.rating;
    const rating_des = decodeURIComponent(req.query.rating_des);
    const comment = decodeURIComponent(req.query.comment);
    const lat = req.query.lat;
    const long = req.query.long;
    const date = req.query.date;

    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {

        const db = client.db('thompson');
        const u_col = db.collection('users');
        const a_col = db.collection('activities');
        const e_col = db.collection('events');

        u_col.findOne({key: key}, (err, query) => {

            console.log(query);

            if(query == null){
                console.log('user key is invalids');
                res.json({'status': res.statusCode, 'return':false,'log':'user key is invalid'});
            } else {
                console.log('user key is valid')

                var patient_activity = {'patient_id': patient_id, 'type': type, 'subtype': subtype, 'rating': rating, 'rating_description': rating_des, 'comment': comment, 'latitude': lat, 'longitude': long, 'date_created': date, 'created': query._id};

                a_col.insertOne(patient_activity, (errr, respond) => {

                    console.log('new record add for patient ' + patient_id)

                    var event_query = {'created_date': date.slice(0,10), 'patient_id': patient_id};

                    e_col.findOne(event_query,(err, event) => {

                        console.log(event);

                        var event_upsert = { $push: {'activities': patient_activity}};

                        e_col.updateMany(event_query, event_upsert, { upsert : true }, (err, respond) => {
                            console.log(respond);
                            res.json({'status': res.statusCode,'return':true,'log':'activity created & inserted to  event'});
                        });

                    });
                });

            }
        });
    });

};

// POST method add sleep to activities and events
exports.add_sleep = (req,res, next) => {

    const patient_id = new ObjectID(req.query.pid);
    const key = req.query.key;
    const duration = req.query.du;
    const depth = req.query.de;
    const interuptions = req.query.inter;
    const snoring = req.query.sn;
    const time_to_sleep = req.query.tts;
    const time_to_up = req.query.ttu;
    const comment = decodeURIComponent(req.query.co);
    const lat = req.query.lat;
    const long = req.query.long;
    const date = req.query.date;

    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {

        const db = client.db('thompson');
        const u_col = db.collection('users');
        const s_col = db.collection('sleeps');
        const e_col = db.collection('events');

        u_col.findOne({key: key}, (err, query) => {

            console.log(query);

            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'log':'user key is invalid'});
            } else {
                console.log('user key is valid')

                var patient_sleep = {'patient_id': patient_id, 'duration': duration, 'depth': depth, 'interuptions': interuptions, 'snoring': snoring, 'time_to_sleep': time_to_sleep, 'time_to_up': time_to_up, 'comment': comment, 'latitude': lat, 'longitude': long, 'date_created': date, 'created': query._id};

                s_col.insertOne(patient_sleep, (errr, respond) => {

                    console.log('new record add for patient ' + patient_id)

                    var event_query = {'created_date': date.slice(0,10), 'patient_id': patient_id};

                    e_col.findOne(event_query,(err, event) => {

                        console.log(event);

                        var event_upsert = { $push: {'sleeps': patient_sleep}};

                        e_col.updateMany(event_query, event_upsert, { upsert : true }, (err, respond) => {
                            console.log(respond);
                            res.json({'status': res.statusCode,'return':true,'log':'sleep created & inserted to  event'});
                        });

                    });
                });

            }
        });

    });

};

// POST method add medicine to medicines and events
exports.add_medicine = (req,res, next) => {

    const patient_id = new ObjectID(req.query.pid);
    const key = req.query.key;
    const name = (req.query.na);
    const strength = decodeURIComponent(req.query.st);
    const quantity = req.query.qu;
    const time_of_day = decodeURIComponent(req.query.tod);
    const comment = decodeURIComponent(req.query.co);
    const lat = req.query.lat;
    const long = req.query.long;
    const date = req.query.date;

    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {

        const db = client.db('thompson');
        const u_col = db.collection('users');
        const m_col = db.collection('medicines');
        const e_col = db.collection('events');

        u_col.findOne({key: key}, (err, query) => {

            console.log(query);

            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'log':'user key is invalid'});
            } else {
                console.log('user key is valid')

                var patient_medicine = {'patient_id': patient_id, 'name': name, 'strength': strength, 'quantity': quantity, 'time_of_day': time_of_day, 'comment': comment, 'latitude': lat, 'longitude': long, 'date_created': date, 'created': query._id};

                m_col.insertOne(patient_medicine, (errr, respond) => {

                    console.log('new record add for patient ' + patient_id)

                    var event_query = {'created_date': date.slice(0,10), 'patient_id': patient_id};

                    e_col.findOne(event_query,(err, event) => {

                        console.log(event);

                        var event_upsert = { $push: {'medicines': patient_medicine}};

                        e_col.updateMany(event_query, event_upsert, { upsert : true }, (err, respond) => {
                            console.log(respond);
                            res.json({'status': res.statusCode,'return':true,'log':'medicine created & inserted to  event'});
                        });

                    });
                });

            }
        });

    });

};

// GET method to retrieve specific patient informaiton
exports.patient_info = (req,res, next) => {

    const patient_id = new ObjectID(req.query.pid);
    const key = req.query.key;

    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {

        const db = client.db('thompson');
        const u_col = db.collection('users');
        const p_col = db.collection('patients');

        var patient_query = {'_id': patient_id};

        u_col.findOne({key: key}, (err, query) => {

            console.log(query);

            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'message':'user key is invalid'});
            } else {
                console.log('user key is valid')

                p_col.findOne(patient_query, (err, result) => {

                    if(result == null){
                        console.log('no paitient was found');
                        res.json({'status': res.statusCode, 'return': false, 'message': 'no paitient was found'})
                    } else {
                        console.log(result);
                        res.json({'status': res.statusCode, 'return': true, 'data': result})
                    }
                });

            }
        });

    });

};

// GET method to retrieve specific patient events
exports.patient_events = (req,res, next) => {

    const patient_id = new ObjectID(req.query.pid);
    const key = req.query.key;

    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {

        const db = client.db('thompson');
        const u_col = db.collection('users');
        const e_col = db.collection('events');

        var patient_query = {'patient_id': patient_id};

        u_col.findOne({key: key}, (err, query) => {

            console.log(query);

            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'message':'user key is invalid'});
            } else {
                console.log('user key is valid')
                e_col.find(patient_query).toArray(function(err, events) {

                    if(events == null){
                        res.json({'status': res.statusCode, 'return': false, 'message': 'cannot find events for this patient'})
                    } else {
                        console.log(events);
                        res.json({'status': res.statusCode, 'return': true, 'data': events})
                    }
                });
            }
        });

    });

};

// GET method to retrieve specific patient device information
exports.patient_providers = (req,res, next) => {

    const patient_id = new ObjectID(req.query.pid);
    const key = req.query.key;

    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {

        const db = client.db('thompson');
        const u_col = db.collection('users');
        const d_col = db.collection('providers');

        var patient_query = {'patient_id': patient_id};

        u_col.findOne({key: key}, (err, query) => {

            console.log(query);

            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'message':'user key is invalid'});
            } else {
                console.log('user key is valid')
                d_col.find(patient_query).toArray(function(err, providers) {

                    console.log(providers);
                    res.json({'status': res.statusCode, 'return': true, 'data': providers})

                });
            }
        });

    });

};

// POST method for user to add 3-party service
exports.add_providers = (req,res, next) => {

    const key = req.query.key;
    const patient_id = new ObjectID(req.query.pid);
    const name = req.query.name;
    const status = req.query.status;
    const authorize = req.query.authorize;
    const access = req.query.access;
    const refresh = req.query.refresh;
    const date = req.query.date;

    var update_provider = { $addToSet: {'providers': {'name': name, 'status': status, 'authorize_token': authorize, 'access_token': access, 'refresh_token': authorize, 'last_sync': date}}};


     mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {

        console.log("------- ADD PROVIDER")

        const db = client.db('thompson');
        const u_col = db.collection('users');
        const d_col = db.collection('providers');

        u_col.findOne({key: key}, (err, query) => {

            console.log(query);

            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'log':'user key is invalid'});
            } else {
                console.log('user key is valid')

                d_col.findOne({'patient_id': patient_id},(err, data) => {

                    console.log(data)

                    if(data != null){

                        d_col.updateMany({'patient_id': patient_id}, update_provider, {upsert : true}, (err, respond) => {
                            console.log(respond);
                            res.json({'status': res.statusCode,'return':true,'log': 'add/update new provider'});
                        });

                    } else {
                        res.json({'status': res.statusCode,'return':false,'log':'cannot add provider'});
                    }



                });

            }
        });

    });
};

// GET method to retrieve specific patient events
exports.all_patients = (req,res, next) => {

    const key = req.query.key;

    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {

        const db = client.db('thompson');
        const u_col = db.collection('users');
        const e_col = db.collection('patients');

        u_col.findOne({key: key}, (err, query) => {

            console.log(query);

            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'message':'user key is invalid'});
            } else {
                console.log('user key is valid')
                e_col.find({}).toArray(function(err, patients) {

                    if(patients == null){
                        res.json({'status': res.statusCode, 'return': false, 'message': 'cannot request all patients'})
                    } else {
                        console.log(patients);
                        res.json({'status': res.statusCode, 'return': true, 'data': patients})
                    }
                });
            }
        });

    });
};

// GET method to retrieve specific patient events
exports.all_users = (req,res, next) => {

    const key = req.query.key;

    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {

        const db = client.db('thompson');
        const u_col = db.collection('users');

        u_col.findOne({key: key}, (err, query) => {

            console.log(query);

            if(query == null){
                console.log('user key is invalid');
                res.json({'status': res.statusCode, 'return':false,'message':'user key is invalid'});
            } else {
                console.log('user key is valid')
                u_col.find({}).toArray(function(err, users) {

                    if(users == null){
                        res.json({'status': res.statusCode, 'return': false, 'message': 'cannot request all users'})
                    } else {
                        console.log(users);
                        res.json({'status': res.statusCode, 'return': true, 'data': users})
                    }
                });
            }
        });

    });
};

// GET method to retrieve server information
exports.server = (req,res, next) => {

  const key = req.query.key;

  mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {

      const db = client.db('thompson');
      const u_col = db.collection('users');

      u_col.findOne({key: key}, (err, query) => {

          console.log(query);

          if(query == null){
              console.log('user key is invalid');
              res.json({'status': res.statusCode, 'return':false,'message':'user key is invalid'});
          } else {
              console.log('user key is valid')
              res.json({'hostname': os.hostname(), 'type': os.type(), 'platform': os.platform(), 'arch': os.arch(),'uptime': os.uptime(), 'totalmem': os.totalmem(), 'freemem': os.freemem(), 'cpu': os.cpus()})
          }
      });

  });
}

exports.patient_delete = (req,res, next) => {

  const key = req.query.key;
  const patient_id = new ObjectID(req.query.pid);

  mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {

    const db = client.db('thompson');
    const p_col = db.collection('patients');
    const u_col = db.collection('users');

    u_col.findOne({key: key}, (err, query) => {

        console.log(query);

        if(query == null){
            console.log('user key is invalid');
            res.json({'status': res.statusCode, 'return':false,'message':'user key is invalid'});
        } else {
            p_col.deleteOne({ '_id': patient_id },function(err, patient) {
                if(patient == null){
                    res.json({'status': res.statusCode, 'return': false, 'message': 'cannot request all users'})
                } else {
                    console.log(patient);
                    res.json({'status': res.statusCode, 'return': true, 'data': patient})
                }
            });
        }
    });

  })
}

exports.user_delete = (req,res, next) => {

  const key = req.query.key;
  const user_id = new ObjectID(req.query.uid);

  mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {

    const db = client.db('thompson');
    const u_col = db.collection('users');

    u_col.findOne({key: key}, (err, query) => {

        console.log(query);

        if(query == null){
            console.log('user key is invalid');
            res.json({'status': res.statusCode, 'return':false,'message':'user key is invalid'});
        } else {
            u_col.deleteOne({ '_id': user_id },function(err, user) {
                if(patient == null){
                    res.json({'status': res.statusCode, 'return': false, 'message': 'cannot request all users'})
                } else {
                    console.log(user);
                    res.json({'status': res.statusCode, 'return': true, 'data': user})
                }
            });
        }
    });

  })
}

// GET method to retrieve specific patient events with date ranges
//exports.patient_events_dateranges = {req, res, next} => {
//
//    const key = req.query.key;
//    const pid = req.query.pid;
//    const startDate = req.query.sd;
//    const endDate = req.query.ed;
//
//    const dateEmpty = {'created_date': date.slice(0,10), 'patient_id': patient_id}
//
//    mongo.connect(mongo_url, {useNewUrlParser: true}, (err, client) => {
//
//        const db = client.db('thompson');
//        const u_col = db.collection('users');
//        const p_col = db.collection('patients');
//        const e_col = db.collection('events');
//
//        u_col.findOne({key: key}, (err, query) => {
//
//            console.log(query);
//
//            if(query == null){
//                console.log('user key is invalid');
//                res.json({'status': res.statusCode, 'return':false,'message':'user key is invalid'});
//            } else {
//                console.log('user key is valid')
//
//
//
//
//            }
//        });
//
//    });
//
//};
