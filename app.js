'use strict'

///////////////////////////////////
///////// Node.js Modules /////////
///////////////////////////////////

// Express frameworks
const express = require('express');
const app = express();
const router = express.Router();

// MongoDB frameworks
//const mongo = require('mongodb').MongoClient;
//const async = require('async');
//const path = require('path');
//const ObjectID = require('mongodb').ObjectID;

// Web-pack frameworks
//const http = require('http');
//const https = require('https');
const assert = require('assert');
//const bodyParser = require('body-parser')
//const axios = require('axios')

// Key and Invite code generators
//const TokenGenerator = require('uuid-token-generator');
//const tokgen2 = new TokenGenerator(256, TokenGenerator.BASE62);
//const shortid = require('shortid');
// Encrypt and decrypt module (https://www.npmjs.com/package/bcrypt)
//const bcrypt = require('bcrypt');
//const saltRounds = 12;
//const lowerCase = require('lower-case')

// Nodemailer
//const nodemailer = require("nodemailer");

///////////////////////////////////
/////////// System Setup //////////
///////////////////////////////////

// Declare Nodejs path and port
//const path = __dirname + '/apps/';
const port = 8080;

app.listen(port, function () {
  console.log('Snickies listening on port 8080!')
})

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/',function(req,res,next){
    res.json({message: 'snickies'});
});

//////////////////////////////////////////////////
//////////////////// Snickies ////////////////////
//////////////////////////////////////////////////

//const poseidon = 'snickies'
//const neptune = require('./apps/snickies');

//////////////////////////////////////////////////
//////////////////// Thompson ////////////////////
//////////////////////////////////////////////////

const poseidon = 'thompson'
const neptune = require('./apps/neptune');

////////// MANUAL DEBUGGING //////////

//app.post('/' + poseidon + '/manual/add_activity', neptune.manual_add_activity);
//app.post('/' + poseidon + '/manual/add_sleep', neptune.manual_add_sleep);
//app.post('/' + poseidon + '/manual/add_medicine', neptune.manual_add_medicine);
//app.get('/' + poseidon + '/credential', neptune.generate);

////////// ENTRY //////////

app.get('/' + poseidon, neptune.index);

////////// PROVIDERS AUTHENTICATION //////////

app.get('/' + poseidon + '/oauth/fitbit/redirect', neptune.oauth_fitbit_redirect)

app.get('/' + poseidon + '/oauth/withings/redirect', neptune.oauth_withings_redirect)

////////// SERVICE APIS //////////

// POST method: request to invite user into application
app.post('/' + poseidon + '/user/invite', neptune.user_invite);

// POST method: with specific invitation code user can request to create account
app.post('/' + poseidon + '/user/signup', neptune.user_signup);

// GET method, user authentication to obtain access key
app.get('/' + poseidon + '/user/signin', neptune.user_signin);

// POST method, user request to reset password
app.post('/' + poseidon + '/user/forgot', neptune.user_forgot);

// GET & POST method, for GET it will request log from specified user, for POST it will post log to specified user
app.post('/' + poseidon + '/user/log', neptune.user_post_log).get('/' + poseidon + '/user/log', neptune.user_get_log);

// GET & POST method, for GET it will retrieve user information, for POST it will update user information
app.post('/' + poseidon + '/user/info', neptune.user_post_info).get('/' + poseidon + '/user/info', neptune.user_get_info);

// POST method create a new patient
app.post('/' + poseidon + '/patient/signup', neptune.patient_signup);

// POST method link patient to specific user
app.post('/' + poseidon + '/patient/link', neptune.patient_link);
app.post('/' + poseidon + '/patient/unlink', neptune.patient_unlink);

// POST method add activity to activities and events
app.post('/' + poseidon + '/patient/add_activity', neptune.add_activity);

app.post('/' + poseidon + '/patient/add_sleep', neptune.add_sleep);

app.post('/' + poseidon + '/patient/add_medicine', neptune.add_medicine);

app.get('/' + poseidon + '/patient/info', neptune.patient_info);

app.get('/' + poseidon + '/patient/events', neptune.patient_events);

app.get('/' + poseidon + '/patient/providers', neptune.patient_providers);

app.post('/' + poseidon + '/patient/add_provider', neptune.add_providers);

////////// Admin endpoints ////////

// GET method, server information
app.get('/' + poseidon + '/admin/server', neptune.server);

// GET method, send all patients
app.get('/' + poseidon + '/admin/patients', neptune.all_patients);

// GET method, send all users
app.get('/' + poseidon + '/admin/users', neptune.all_users);

// GET method, send all users
app.get('/' + poseidon + '/admin/patient/delete', neptune.patient_delete);

// GET method, send all users
app.get('/' + poseidon + '/admin/user/delete', neptune.user_delete);

//app.get('/' + poseidon + '/patient/events/date_ranges', neptune.patient_events_dateranges);
