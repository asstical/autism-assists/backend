# Capturing and Managing Daily Symptoms Data in the Treatment of Autism Spectrum Disorder Using Mobile Technology: A Pilot Study - Backend

Node.js powered backend with MongoDB as a database. Hosted on Amazon Webservices GovCloud and security for HIPAA Compliance. You will only be able to access Assists API endpoints only if you have the key which is created only on request.

Please email [thunpisit.am@gmail.com , thunpisit@mail.missouri.edu] for information to access API endpoints.

# Requirements
* Node.js
* MongoDB

# Get Started
* npm install
* node index.js

# Developer
* **Thunpisit Amnuaikiatloet** (thunpisit.am@gmail.com , thunpisit@mail.missouri.edu)
* **Fang Wang** (wangfan@missouri.edu)
