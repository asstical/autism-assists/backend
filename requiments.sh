#!/bin/bash

# Create NodeJS environment
sudo apt update
sudo apt install nodejs -y
sudo apt install npm -y

# Install main moduels
npm install express request http https assert router --save-dev -y

# Install MongoDB and etc modules
npm install mongodb async path uuid-token-generator shortid lower-case axios bcryptjs --save-dev -y

# Install Nodemailer
npm install nodemailer --save-dev -y
